#' The 450k data of buccal invariant sites
#'
#' A list of probes of 450k DNA methylation data that are invariant for buccal cells.
#'
#' @docType data
#'
#' @usage data(buccalInvariant)
#'
#' @format An object of class \code{"character"}; see \code{\link[base]{character}}.
#'
#' @keywords datasets
#'
#' @references Edgar, R. D., Jones, M. J., Robinson, W. P., & Kobor, M. S. (2017). An empirically driven data reduction method on the human 450K methylation array to remove tissue specific non-variable CpGs. Clinical epigenetics, 9(1), 11.
#' (\href{http://www.ncbi.nlm.nih.gov/pubmed/28184257}{PubMed})
#'
#' @source \href{https://raw.githubusercontent.com/redgar598/Tissue_Nonvariable_450K_CpGs/master/Invariant_Buccal_CpGs.csv}{CSV file}
#'
#' @examples
#' buccalInvariantSites <- data(buccalInvariant)
#' buccalInvariantSites
"buccalInvariant"
