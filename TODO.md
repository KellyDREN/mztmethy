# `getTwinPairs()`

- Write this as an `S4` method 
    - First we need to define a generic, which allows us to specify which metadata column contains the twin information
    - `setGeneric("getTwinPairs", function(x, twinCol, sexCol){standardGeneric("getTwinPairs")})
    - `setMethod("getTwinPairs", signature = "GenomicRatioSet",
    function(x, twinCol, sexCol){
    # stuff
    # check twinCol exists in the colData(x)
    # check each twin only appears twice
    })`
    - setMethod("getTwinPairs", signature = "GenomicMethylSet",
    function(x, twinCol, sexCol){
    # stuff
    })`
    
# `makeInvariantCdf()`

- Change to an `S4` method
